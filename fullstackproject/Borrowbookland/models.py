from contextlib import nullcontext
from distutils.command.upload import upload
from email.headerregistry import Address
from email.policy import default
from math import fabs
from pyexpat import model
from django.db import models

# Create your models here.
class LendBookUser(models.Model):
    name = models.CharField(max_length=50,null=True)
    emailId = models.EmailField()
    phoneNo = models.CharField(max_length=10,null=True)
    nameOfBook = models.CharField(max_length=200,null=True)
    authorOfBook = models.CharField(max_length=50,null=True)
    genre = models.CharField(max_length=100,null=True)
    image = models.ImageField(default=True,upload_to='pics')
    address = models.CharField(max_length=200,null=True)
    def __str__(self):
        return f"{self.nameOfBook}"
    class Meta:
        unique_together = ["name", "emailId", "phoneNo","nameOfBook","authorOfBook","genre","image","address"]

class StoreInformation(models.Model):
    image = models.ImageField(default=True,upload_to='pics')
    name = models.CharField(max_length=100,null=True)
    phoneNo = models.CharField(max_length=10,null=True)
    address = models.CharField(max_length=200,null=True)
    def __str__(self):
        return f"{self.name}"

class SellBookUser(models.Model):
    name = models.CharField(max_length=50,null=True)
    emailId = models.EmailField()
    image = models.ImageField(default=True,upload_to='pics')
    phoneNo = models.CharField(max_length=10,null=True)
    nameOfBook = models.CharField(max_length=200,null=True)
    authorOfBook = models.CharField(max_length=50,null=True)
    genre = models.CharField(max_length=100,null=True)
    address = models.CharField(max_length=200,null=True)
    def __str__(self):
        return f"{self.name}"

class BorrowBookUser(models.Model):
    name = models.CharField(max_length=50,null=True)
    emailId = models.EmailField()
    phoneNo = models.CharField(max_length=10,null=True)
    nameOfBook = models.CharField(max_length=200,null=True)
    authorOfBook = models.CharField(max_length=50,null=True)
    genre = models.CharField(max_length=100,null=True)
    address = models.CharField(max_length=200,null=True)
    def __str__(self):
        return f"{self.name}"

class ContactUs(models.Model):
    name = models.CharField(max_length=50,null=True)
    emailId = models.EmailField()
    query = models.CharField(max_length=300,null=True)
    def __str__(self):
        return f"{self.name}"

