from django.urls import path
from . import views

urlpatterns = [
    path('index.html', views.index, name='index'),
    path('sellBooks.html', views.sellBooks, name='sellbooks'),
    path('aboutUs.html', views.aboutUs, name='aboutUs'),
    path('borrowBooks.html', views.borrowBooks, name='borrowBooks'),
    path('contactUs.html', views.contactUs, name='contactUs'),
    path('lendBooks.html', views.lendBooks, name='lendBooks'),
    path('booklended', views.booklended, name='booklended'),
    path('booksell', views.booksell, name='booksell'),
    path('bookborrow', views.bookborrow, name='bookborrow'),
    path('contactUs', views.contactus, name='contactus'),
    path('searchstore', views.searchstore, name='searchstore'),
    path('borrowsearch', views.borrowsearch, name='borrowsearch'),
]