# Generated by Django 4.0.4 on 2022-05-25 04:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Borrowbookland', '0002_rename_lendbookusers_lendbookuser'),
    ]

    operations = [
        migrations.AddField(
            model_name='lendbookuser',
            name='name',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
