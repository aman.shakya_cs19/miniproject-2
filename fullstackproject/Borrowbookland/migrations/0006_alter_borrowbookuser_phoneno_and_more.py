# Generated by Django 4.0.4 on 2022-05-25 05:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Borrowbookland', '0005_borrowbookuser_contactus_sellbookuser_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='borrowbookuser',
            name='phoneNo',
            field=models.CharField(max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='lendbookuser',
            name='phoneNo',
            field=models.CharField(max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='sellbookuser',
            name='phoneNo',
            field=models.CharField(max_length=10, null=True),
        ),
        migrations.AlterField(
            model_name='storeinformation',
            name='phoneNo',
            field=models.CharField(max_length=10, null=True),
        ),
    ]
