from django.shortcuts import render
from django.http import HttpResponse
from .models import BorrowBookUser, ContactUs, LendBookUser, SellBookUser, StoreInformation


def index(request):
    return render(request, 'index.html')

def sellBooks(request):
    stores = StoreInformation.objects.all()
    return render(request,'sellBooks.html',{"stores":stores})

def aboutUs(request):
    return render(request,'aboutUs.html')

def borrowBooks(request):
    books = LendBookUser.objects.all()
    return render(request,'borrowBooks.html',{"books":books})

def contactUs(request):
    return render(request,'contactUs.html')

def lendBooks(request):
    return render(request,'lendBooks.html')

def booklended(request):
    name = request.POST.get('name')
    emailID = request.POST.get('email')
    phoneNo = request.POST.get('contact')
    nameOfBook = request.POST.get('name_of_book')
    authorOfBook = request.POST.get('author_of_book')
    genre = request.POST.get('genre')
    image = request.FILES['upload_book']
    address = request.POST.get('location')
    book = LendBookUser(name=name,emailId=emailID,phoneNo=phoneNo,nameOfBook=nameOfBook,authorOfBook=authorOfBook,genre=genre,image=image,address=address)
    book.save() 
    books = LendBookUser.objects.all()
    return render(request,'borrowBooks.html',{"books":books})

def booksell(request):
    name = request.POST.get('name')
    emailID = request.POST.get('email')
    phoneNo = request.POST.get('contact')
    nameOfBook = request.POST.get('name_of_book')
    authorOfBook = request.POST.get('author_of_book')
    genre = request.POST.get('genre')
    image = request.FILES['upload_book']
    address = request.POST.get('location')
    book = SellBookUser(name=name,emailId=emailID,phoneNo=phoneNo,nameOfBook=nameOfBook,authorOfBook=authorOfBook,genre=genre,image=image,address=address)
    book.save() 
    return render(request,'index.html')

def bookborrow(request):
    name = request.POST.get('name')
    emailID = request.POST.get('email')
    phoneNo = request.POST.get('contact')
    nameOfBook = request.POST.get('name_of_book')
    authorOfBook = request.POST.get('author_of_book')
    genre = request.POST.get('genre')
    image = request.FILES['upload_book']
    address = request.POST.get('location')
    book = BorrowBookUser(name=name,emailId=emailID,phoneNo=phoneNo,nameOfBook=nameOfBook,authorOfBook=authorOfBook,genre=genre,image=image,address=address)
    book.save() 
    return render(request,'index.html')

def contactus(request):
    name = request.POST.get('name')
    emailID = request.POST.get('email')
    query = request.POST.get('Query')
    book = ContactUs(name=name,emailId=emailID,query=query)
    book.save() 
    return render(request,'index.html')

def searchstore(request):
    stores = StoreInformation.objects.all()
    area = request.POST.get('search')
    stores = stores.filter(address__icontains = area)
    return render(request,'sellBooks.html',{"stores":stores})

def borrowsearch(request):
    books = LendBookUser.objects.all()
    area = request.POST.get('search')
    books = books.filter(nameOfBook__icontains = area)
    return render(request,'borrowBooks.html',{"books":books})