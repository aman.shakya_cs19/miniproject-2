from django.contrib import admin
from django.contrib.admin import AdminSite
from  .models import LendBookUser
from  .models import StoreInformation
from  .models import SellBookUser
from  .models import BorrowBookUser
from  .models import ContactUs
# Register your models here.

admin.site.register(LendBookUser)
admin.site.register(StoreInformation)
admin.site.register(SellBookUser)
admin.site.register(BorrowBookUser)
admin.site.register(ContactUs)